/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dist/";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	eval("module.exports = __webpack_require__(1);\n\n\n//////////////////\n// WEBPACK FOOTER\n// multi main\n// module id = 0\n// module chunks = 0\n//# sourceURL=webpack:///multi_main?");

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	eval("'use strict';\n\nvar _helpers = __webpack_require__(2);\n\n// Получение всех аудиозаписей\nvar audios = (0, _helpers.getAllAudios)();\n\n// Нажимаем на все аудиозаписи\naudios.forEach(function (audio) {\n  return (0, _helpers.eventFire)(audio, 'click');\n});\n\n//////////////////\n// WEBPACK FOOTER\n// ./src/main.js\n// module id = 1\n// module chunks = 0\n//# sourceURL=webpack:///./src/main.js?");

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	eval("'use strict';\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\nexports.getAllAudios = exports.eventFire = undefined;\n\nvar _eventFire = __webpack_require__(3);\n\nvar _eventFire2 = _interopRequireDefault(_eventFire);\n\nvar _getAllAudios = __webpack_require__(4);\n\nvar _getAllAudios2 = _interopRequireDefault(_getAllAudios);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nexports.eventFire = _eventFire2.default;\nexports.getAllAudios = _getAllAudios2.default;\n\n//////////////////\n// WEBPACK FOOTER\n// ./src/helpers/index.js\n// module id = 2\n// module chunks = 0\n//# sourceURL=webpack:///./src/helpers/index.js?");

/***/ },
/* 3 */
/***/ function(module, exports) {

	eval("'use strict';\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\n/**\n * Функция, которая создает простую работу с event'ами\n *\n * @param {Element} el - элемент\n * @param {String} etype - тип event'а\n */\nvar eventFire = function eventFire(el, etype) {\n    if (el.fireEvent) {\n        el.fireEvent('on' + etype);\n    } else {\n        var evObject = document.createEvent('Events');\n        evObject.initEvent(etype, true, false);\n        el.dispatchEvent(evObject);\n    }\n};\n\nexports.default = eventFire;\n\n//////////////////\n// WEBPACK FOOTER\n// ./src/helpers/event-fire.js\n// module id = 3\n// module chunks = 0\n//# sourceURL=webpack:///./src/helpers/event-fire.js?");

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	eval("'use strict';\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\n\nvar _eventFire = __webpack_require__(3);\n\nvar _eventFire2 = _interopRequireDefault(_eventFire);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nvar iterates = 0;\n\n/**\n * Получение всех аудиозаписей\n *\n * @returns {NodeList}\n */\nvar getAllAudios = function getAllAudios() {\n    // До тех пор, пока не подгрузили все аудиозаписи\n    while (document.querySelector('.olist_more.flat_button.secondary') || iterates > 100) {\n        // Кнопка для подгрузки данных\n        var $more = document.querySelector('.olist_more.flat_button.secondary');\n        (0, _eventFire2.default)($more, 'click');\n\n        iterates += 1;\n    }\n\n    return document.querySelectorAll('.olist_item_wrap');\n};\n\nexports.default = getAllAudios;\n\n//////////////////\n// WEBPACK FOOTER\n// ./src/helpers/get-all-audios.js\n// module id = 4\n// module chunks = 0\n//# sourceURL=webpack:///./src/helpers/get-all-audios.js?");

/***/ }
/******/ ]);