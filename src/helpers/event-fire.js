/**
 * Функция, которая создает простую работу с event'ами
 *
 * @param {Element} el - элемент
 * @param {String} etype - тип event'а
 */
const eventFire = (el, etype) => {
    if (el.fireEvent) {
        el.fireEvent(`on${etype}`);
    } else {
        const evObject = document.createEvent('Events');
        evObject.initEvent(etype, true, false);
        el.dispatchEvent(evObject);
    }
};

export default eventFire;
