import eventFire         from './event-fire';

let iterates = 0;

/**
 * Получение всех аудиозаписей
 *
 * @returns {NodeList}
 */
const getAllAudios = () => {
    // До тех пор, пока не подгрузили все аудиозаписи
    while ( document.querySelector('.olist_more.flat_button.secondary') || iterates > 100 ) {
        // Кнопка для подгрузки данных
        const $more = document.querySelector('.olist_more.flat_button.secondary');
        eventFire($more, 'click');

        iterates += 1;
    }

    return document.querySelectorAll('.olist_item_wrap');
};

export default getAllAudios;
