import eventFire            from './event-fire';
import getAllAudios         from './get-all-audios';

export {
    eventFire,
    getAllAudios
};
