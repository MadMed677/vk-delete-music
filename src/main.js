import { eventFire, getAllAudios }           from './helpers';

// Получение всех аудиозаписей
const audios = getAllAudios();

// Нажимаем на все аудиозаписи
audios.forEach(audio => eventFire(audio, 'click'));
